# anomalies

Usage:


```go
package main

import (
    "gitlab.com/felipemocruha/anomalies"
)

func main() {
    if err := CallDatabase(); err != nil {
        anomaly := anomalies.Internal(err)
        anomaly.Log("failed to call database")
        
        code, resp := anomalies.ToResponse(anomaly)
        // or
        respErr := anomalies.ToGrpcResponse(anomaly)
    } 
    ...
}
```
