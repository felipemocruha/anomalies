module gitlab.com/felipemocruha/anomalies

go 1.15

require (
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.20.0
	github.com/stretchr/testify v1.6.1
	google.golang.org/grpc v1.33.2
)
