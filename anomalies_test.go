package anomalies

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func TestValidationError(t *testing.T) {
	msg := "some error"
	anomaly := Validation(errors.New(msg))

	want := fmt.Sprintf("%v: %v", VALIDATION, msg)
	assert.Equal(t, want, anomaly.Error())
	assert.Equal(t, VALIDATION, anomaly.Category())
}

func TestValidationErrorWithExtraMsg(t *testing.T) {
	msg := "some error"
	extra := "extra"
	anomaly := Validation(errors.New(msg), extra)

	want := fmt.Sprintf("%v: %v: %v", VALIDATION, extra, msg)
	assert.Equal(t, want, anomaly.Error())
	assert.Equal(t, VALIDATION, anomaly.Category())
}

func TestValidationErrorToResponse(t *testing.T) {
	msg := "some error"
	anomaly := Validation(errors.New(msg))

	code, payload := ToResponse(anomaly)
	assert.Equal(t, 400, code)

	want := ErrorResponse{
		Category: VALIDATION,
		Message:  fmt.Sprintf("%v: %v", VALIDATION, msg),
	}
	assert.Equal(t, want, payload)
}

func TestValidationErrorToGrpcResponse(t *testing.T) {
	msg := "some error"
	anomaly := Validation(errors.New(msg))

	resp := ToGrpcResponse(anomaly)
	st, _ := status.FromError(resp)

	assert.Equal(t, codes.InvalidArgument, st.Code())
	assert.Equal(t, anomaly.Error(), st.Message())
}

func TestAuthenticationError(t *testing.T) {
	msg := "some error"
	anomaly := Authentication(errors.New(msg))

	want := fmt.Sprintf("%v: %v", AUTHENTICATION, msg)
	assert.Equal(t, want, anomaly.Error())
	assert.Equal(t, AUTHENTICATION, anomaly.Category())
}

func TestAuthenticationErrorWithExtraMsg(t *testing.T) {
	msg := "some error"
	extra := "extra"
	anomaly := Authentication(errors.New(msg), extra)

	want := fmt.Sprintf("%v: %v: %v", AUTHENTICATION, extra, msg)
	assert.Equal(t, want, anomaly.Error())
	assert.Equal(t, AUTHENTICATION, anomaly.Category())
}

func TestAuthenticationErrorToResponse(t *testing.T) {
	msg := "some error"
	anomaly := Authentication(errors.New(msg))

	code, payload := ToResponse(anomaly)
	assert.Equal(t, 401, code)

	want := ErrorResponse{
		Category: AUTHENTICATION,
		Message:  fmt.Sprintf("%v: %v", AUTHENTICATION, msg),
	}
	assert.Equal(t, want, payload)
}

func TestAuthenticationErrorToGrpcResponse(t *testing.T) {
	msg := "some error"
	anomaly := Authentication(errors.New(msg))

	resp := ToGrpcResponse(anomaly)
	st, _ := status.FromError(resp)

	assert.Equal(t, codes.Unauthenticated, st.Code())
	assert.Equal(t, anomaly.Error(), st.Message())
}

func TestAuthorizationError(t *testing.T) {
	msg := "some error"
	anomaly := Authorization(errors.New(msg))

	want := fmt.Sprintf("%v: %v", AUTHORIZATION, msg)
	assert.Equal(t, want, anomaly.Error())
	assert.Equal(t, AUTHORIZATION, anomaly.Category())
}

func TestAuthorizationErrorWithExtraMsg(t *testing.T) {
	msg := "some error"
	extra := "extra"
	anomaly := Authorization(errors.New(msg), extra)

	want := fmt.Sprintf("%v: %v: %v", AUTHORIZATION, extra, msg)
	assert.Equal(t, want, anomaly.Error())
	assert.Equal(t, AUTHORIZATION, anomaly.Category())
}

func TestAuthorizationErrorToResponse(t *testing.T) {
	msg := "some error"
	anomaly := Authorization(errors.New(msg))

	code, payload := ToResponse(anomaly)
	assert.Equal(t, 403, code)

	want := ErrorResponse{
		Category: AUTHORIZATION,
		Message:  fmt.Sprintf("%v: %v", AUTHORIZATION, msg),
	}
	assert.Equal(t, want, payload)
}

func TestAuthorizationErrorToGrpcResponse(t *testing.T) {
	msg := "some error"
	anomaly := Authorization(errors.New(msg))

	resp := ToGrpcResponse(anomaly)
	st, _ := status.FromError(resp)

	assert.Equal(t, codes.PermissionDenied, st.Code())
	assert.Equal(t, anomaly.Error(), st.Message())
}

func TestNotFoundError(t *testing.T) {
	msg := "some error"
	anomaly := NotFound(errors.New(msg))

	want := fmt.Sprintf("%v: %v", NOT_FOUND, msg)
	assert.Equal(t, want, anomaly.Error())
	assert.Equal(t, NOT_FOUND, anomaly.Category())
}

func TestNotFoundErrorWithExtraMsg(t *testing.T) {
	msg := "some error"
	extra := "extra"
	anomaly := NotFound(errors.New(msg), extra)

	want := fmt.Sprintf("%v: %v: %v", NOT_FOUND, extra, msg)
	assert.Equal(t, want, anomaly.Error())
	assert.Equal(t, NOT_FOUND, anomaly.Category())
}

func TestNotFoundErrorToResponse(t *testing.T) {
	msg := "some error"
	anomaly := NotFound(errors.New(msg))

	code, payload := ToResponse(anomaly)
	assert.Equal(t, 404, code)

	want := ErrorResponse{
		Category: NOT_FOUND,
		Message:  fmt.Sprintf("%v: %v", NOT_FOUND, msg),
	}
	assert.Equal(t, want, payload)
}

func TestNotFoundErrorToGrpcResponse(t *testing.T) {
	msg := "some error"
	anomaly := NotFound(errors.New(msg))

	resp := ToGrpcResponse(anomaly)
	st, _ := status.FromError(resp)

	assert.Equal(t, codes.NotFound, st.Code())
	assert.Equal(t, anomaly.Error(), st.Message())
}

func TestInternalError(t *testing.T) {
	msg := "some error"
	anomaly := Internal(errors.New(msg))

	want := fmt.Sprintf("%v: %v", INTERNAL, msg)
	assert.Equal(t, want, anomaly.Error())
	assert.Equal(t, INTERNAL, anomaly.Category())
}

func TestInternalErrorWithExtraMsg(t *testing.T) {
	msg := "some error"
	extra := "extra"
	anomaly := Internal(errors.New(msg), extra)

	want := fmt.Sprintf("%v: %v: %v", INTERNAL, extra, msg)
	assert.Equal(t, want, anomaly.Error())
	assert.Equal(t, INTERNAL, anomaly.Category())
}

func TestInternalErrorToResponse(t *testing.T) {
	msg := "some error"
	anomaly := Internal(errors.New(msg))

	code, payload := ToResponse(anomaly)
	assert.Equal(t, 500, code)

	want := ErrorResponse{
		Category: INTERNAL,
		Message:  fmt.Sprintf("%v: %v", INTERNAL, msg),
	}
	assert.Equal(t, want, payload)
}

func TestInternalErrorToGrpcResponse(t *testing.T) {
	msg := "some error"
	anomaly := Internal(errors.New(msg))

	resp := ToGrpcResponse(anomaly)
	st, _ := status.FromError(resp)

	assert.Equal(t, codes.Internal, st.Code())
	assert.Equal(t, anomaly.Error(), st.Message())
}
