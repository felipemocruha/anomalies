package anomalies

import (
	"fmt"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

// TODO: add missing permissions
type AuthorizationError struct {
	Err error `json:"message"`
}

// TODO: format to print missing permissions
func (e AuthorizationError) Error() string {
	return fmt.Sprintf("%v: %v", AUTHORIZATION, e.Err.Error())
}

func (e AuthorizationError) Category() ErrorCategory {
	return AUTHORIZATION
}

func Authorization(err error, msg ...string) Anomaly {
	if len(msg) == 0 {
		return &AuthorizationError{err}
	}

	return &AuthorizationError{errors.Wrap(err, msg[0])}
}

func (e AuthorizationError) Log(msg string) {
	log.Error().
		Str("category", string(AUTHORIZATION)).
		Msgf("%v: %v", msg, e.Err.Error())
}
