package anomalies

import (
	"fmt"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

type ValidationError struct {
	Err error `json:"message"`
}

func (e ValidationError) Error() string {
	return fmt.Sprintf("%v: %v", VALIDATION, e.Err.Error())
}

func (e ValidationError) Category() ErrorCategory {
	return VALIDATION
}

func Validation(err error, msg ...string) Anomaly {
	if len(msg) == 0 {
		return &ValidationError{err}
	}

	return &ValidationError{errors.Wrap(err, msg[0])}
}

func (e ValidationError) Log(msg string) {
	log.Error().
		Str("category", string(VALIDATION)).
		Msgf("%v: %v", msg, e.Err.Error())
}
