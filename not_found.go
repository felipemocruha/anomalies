package anomalies

import (
	"fmt"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

type NotFoundError struct {
	Err error `json:"message"`
}

func (e NotFoundError) Error() string {
	return fmt.Sprintf("%v: %v", NOT_FOUND, e.Err.Error())
}

func (e NotFoundError) Category() ErrorCategory {
	return NOT_FOUND
}

func NotFound(err error, msg ...string) Anomaly {
	if len(msg) == 0 {
		return &NotFoundError{err}
	}

	return &NotFoundError{errors.Wrap(err, msg[0])}
}

func (e NotFoundError) Log(msg string) {
	log.Error().
		Str("category", string(NOT_FOUND)).
		Msgf("%v: %v", msg, e.Err.Error())
}
