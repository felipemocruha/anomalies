package anomalies

import (
	"fmt"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

type InternalError struct {
	Err error `json:"message"`
}

func (e InternalError) Error() string {
	return fmt.Sprintf("%v: %v", INTERNAL, e.Err.Error())
}

func (e InternalError) Category() ErrorCategory {
	return INTERNAL
}

func Internal(err error, msg ...string) Anomaly {
	if len(msg) == 0 {
		return &InternalError{err}
	}

	return &InternalError{errors.Wrap(err, msg[0])}
}

func (e InternalError) Log(msg string) {
	log.Error().
		Str("category", string(INTERNAL)).
		Msgf("%v: %v", msg, e.Err.Error())
}
