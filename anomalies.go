package anomalies

import (
	"github.com/pkg/errors"
)

type ErrorCategory string

const (
	VALIDATION     ErrorCategory = "validation_error"
	AUTHENTICATION ErrorCategory = "authentication_error"
	AUTHORIZATION  ErrorCategory = "authorization_error"
	NOT_FOUND      ErrorCategory = "not_found_error"
	INTERNAL       ErrorCategory = "internal_error"
)

type Anomaly interface {
	error
	Category() ErrorCategory
	Log(msg string)
}

type ErrorResponse struct {
	Category ErrorCategory `json:"error_category"`
	Message  string        `json:"message"`
}

func ToResponse(anomaly Anomaly) (int, interface{}) {
	switch anomaly.Category() {
	case VALIDATION:
		return 400, ErrorResponse{
			Category: VALIDATION,
			Message:  anomaly.Error(),
		}

	case AUTHENTICATION:
		return 401, ErrorResponse{
			Category: AUTHENTICATION,
			Message:  anomaly.Error(),
		}

	case AUTHORIZATION:
		return 403, ErrorResponse{
			Category: AUTHORIZATION,
			Message:  anomaly.Error(),
		}

	case NOT_FOUND:
		return 404, ErrorResponse{
			Category: NOT_FOUND,
			Message:  anomaly.Error(),
		}

	case INTERNAL:
		return 500, ErrorResponse{
			Category: INTERNAL,
			Message:  anomaly.Error(),
		}

	}

	return 500, errors.New("Internal Error")
}
