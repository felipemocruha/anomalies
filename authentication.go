package anomalies

import (
	"fmt"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

type AuthenticationError struct {
	Err error `json:"message"`
}

func (e AuthenticationError) Error() string {
	return fmt.Sprintf("%v: %v", AUTHENTICATION, e.Err.Error())
}

func (e AuthenticationError) Category() ErrorCategory {
	return AUTHENTICATION
}

func Authentication(err error, msg ...string) Anomaly {
	if len(msg) == 0 {
		return &AuthenticationError{err}
	}

	return &AuthenticationError{errors.Wrap(err, msg[0])}
}

func (e AuthenticationError) Log(msg string) {
	log.Error().
		Str("category", string(AUTHENTICATION)).
		Msgf("%v: %v", msg, e.Err.Error())
}
