package anomalies

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func ToGrpcResponse(anomaly Anomaly) error {
	switch anomaly.Category() {
	case VALIDATION:
		return status.Error(codes.InvalidArgument, anomaly.Error())

	case AUTHENTICATION:
		return status.Error(codes.Unauthenticated, anomaly.Error())

	case AUTHORIZATION:
		return status.Error(codes.PermissionDenied, anomaly.Error())

	case NOT_FOUND:
		return status.Error(codes.NotFound, anomaly.Error())

	case INTERNAL:
		return status.Error(codes.Internal, anomaly.Error())

	}

	return status.Error(codes.Internal, anomaly.Error())
}
